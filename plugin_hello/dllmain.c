#include <strsafe.h>

#include "framework.h"
#include "stdio.h"

__declspec(dllexport) void __cdecl Execute() {
	printf("Hello, world!\n");
}

char authorMessage[] = "Herman <herman@spiralarms.org>";
char descriptionMessage[] = "Prints \"Hello, world!\"";

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message);

__declspec(dllexport) BOOLEAN __cdecl GetAuthor(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, authorMessage);
}

__declspec(dllexport) BOOLEAN __cdecl GetDescription(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, descriptionMessage);
}

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message) {
	HRESULT result = StringCchCopy(buffer, dwBufferSize, message);
	*pdwBytesWritten = min(dwBufferSize, strlen(message) * sizeof(char));
	return result == S_OK;
}