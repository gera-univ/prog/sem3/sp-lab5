#include "framework.h"
#include <strsafe.h>
#include <tchar.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tlhelp32.h>
#include "../ps-lib/ps-lib.h"

__declspec(dllexport) void __cdecl Execute() {
	const HANDLE process_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	size_t process_cnt;
	GetMyProcessList(process_snapshot_handle, NULL, &process_cnt);
	if (process_cnt == 0) {
		perror("Incorrect process count");
		exit(1);
	}

	struct MyProcess* process_list = malloc(sizeof(struct MyProcess) * process_cnt);
	GetMyProcessList(process_snapshot_handle, process_list, NULL);
	if (process_list == NULL) {
		perror("Couldn't get process list");
		exit(1);
	}

	printf("User\t\tPID\t\tParent\t\tPriority\tThreads\t\tModules\n");
	for (size_t i = 0; i < process_cnt; ++i) {
		const HANDLE module_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, process_list[i].pid);
		const HANDLE thread_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);

		size_t module_cnt;
		GetMyModuleList(module_snapshot_handle, NULL, &module_cnt);

		size_t thread_cnt;
		GetMyThreadList(thread_snapshot_handle, process_list[i].pid, NULL, &thread_cnt);

		printf("%lu\t\t%lu\t\t%lu\t\t%lu\t\t%zu\t\t%zu\n",
		       process_list[i].pid,
		       process_list[i].parent_id,
		       process_list[i].priority_class,
		       process_list[i].thread_count,
			module_cnt, thread_cnt);

		CloseHandle(module_snapshot_handle);
		CloseHandle(thread_snapshot_handle);
	}

	free(process_list);
	CloseHandle(process_snapshot_handle);
}

char authorMessage[] = "Herman <herman@spiralarms.org>";
char descriptionMessage[] = "Prints process list";

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message);

__declspec(dllexport) BOOLEAN __cdecl GetAuthor(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, authorMessage);
}

__declspec(dllexport) BOOLEAN __cdecl GetDescription(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, descriptionMessage);
}

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message) {
	HRESULT result = StringCchCopy(buffer, dwBufferSize, message);
	*pdwBytesWritten = min(dwBufferSize, strlen(message) * sizeof(char));
	return result == S_OK;
}
