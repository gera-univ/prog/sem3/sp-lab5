# Лабораторная работа №5 по СП "Создание и использование DLL"

- Приложение: *modular_thing*
- Плагины: *malicious_plugin, plugin_hello, plugin_process_lister, plugin_volume_lister*
- Cтатические библиотеки: *dll_lister, ps-lib*
- Библиотеки с неявным связыванием: *change_colour*
- Локализация с использованием ~~ресурсов библиотеки~~ экспортированных строк: *lang_be_BY, lang_ru_RU*

Поддерживается только 32-битная архитектура, no name mangling.

Ввывод `--help`:
```
View info about and execute DLL plugins.

Options:
  -h, --help            display help message
  -d, --dry-run         do not execute plugins, only print info, list languages even if the following option is unspecified
  -L, --lang            set the language, must be equal to the Lang_Name variable exported in the language plugin
  --lang-dir            relative path to the directory too search for language plugins in (default is "lang")
  --plugin-dir          relative path to the directory too search for plugins in (default is "plugins")


Plugin DLLs should export the following functions:
__declspec(dllexport) BOOLEAN __cdecl GetAuthor(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten)
__declspec(dllexport) BOOLEAN __cdecl GetDescription(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten)
__declspec(dllexport) void __cdecl Execute()


Language DLLs should export the following variables:
__declspec(dllexport) char __cdecl Lang_Name[]
__declspec(dllexport) char __cdecl Loc_PluginInfo[]
__declspec(dllexport) char __cdecl Loc_LanguageSet[]
__declspec(dllexport) char __cdecl Loc_NumLoadedPlugins[]
__declspec(dllexport) char __cdecl Loc_LoadingPlugins[]
```
