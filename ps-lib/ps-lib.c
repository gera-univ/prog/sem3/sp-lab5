#include "ps-lib.h"

#include <tlhelp32.h>
#include <stdlib.h>

void GetMyProcessList(HANDLE snapshot_handle, struct MyProcess* process_list, size_t* ret_process_count) {
	PROCESSENTRY32* process_entry = malloc(sizeof(PROCESSENTRY32));
	if (process_entry == NULL) {
		perror("Couldn't allocate process_entry");
		exit(1);
	}
	process_entry->dwSize = sizeof(PROCESSENTRY32);

	Process32First(snapshot_handle, process_entry);

	size_t i = 0;
	do {
		if (process_list != NULL) {
			process_list[i].pid = process_entry->th32ProcessID;
			process_list[i].parent_id = process_entry->th32ParentProcessID;
			process_list[i].thread_count = process_entry->cntThreads;
			const HANDLE process_handle = OpenProcess(PROCESS_ALL_ACCESS, TRUE, process_list[i].pid);
			process_list[i].priority_class = GetPriorityClass(process_handle);
			CloseHandle(process_handle);
		}
		++i;
	}
	while (Process32Next(snapshot_handle, process_entry));
	if (ret_process_count != NULL)
		*ret_process_count = i;
}

void GetMyModuleList(HANDLE snapshot_handle, struct MyModule* module_list, size_t* ret_module_count) {
	MODULEENTRY32* module_entry = malloc(sizeof(MODULEENTRY32));
	if (module_entry == NULL) {
		perror("Couldn't allocate module_entry");
		exit(1);
	}
	module_entry->dwSize = sizeof(MODULEENTRY32);

	Module32First(snapshot_handle, module_entry);

	size_t i = 0;
	do {
		if (module_list != NULL) {
			const size_t name_length = wcslen(module_entry->szModule) + 4;
			module_list[i].name = malloc(sizeof(wchar_t) * (name_length));
			if (module_list[i].name == NULL) {
				perror("Couldn't allocate memory for module name string");
				exit(1);
			}
			wcsncpy_s(module_list[i].name, name_length, module_entry->szModule, name_length);

			const size_t exe_path_length = wcslen(module_entry->szExePath) + 4;
			module_list[i].exe_path = malloc(sizeof(wchar_t) * (exe_path_length));
			if (module_list[i].exe_path == NULL) {
				perror("Couldn't allocate memory for module exe path string");
				exit(1);
			}
			wcsncpy_s(module_list[i].exe_path, exe_path_length, module_entry->szExePath, exe_path_length);

			module_list[i].base_size = module_entry->modBaseSize;
		}
		++i;
	}
	while (Module32Next(snapshot_handle, module_entry));
	if (ret_module_count != NULL)
		*ret_module_count = i;

	free(module_entry);
}

void GetMyThreadList(HANDLE snapshot_handle, DWORD pid, struct MyThread* thread_list, size_t* ret_thread_count) {
	THREADENTRY32* thread_entry = malloc(sizeof(THREADENTRY32));
	if (thread_entry == NULL) {
		perror("Couldn't allocate thread_entry");
		exit(1);
	}
	thread_entry->dwSize = sizeof(THREADENTRY32);

	size_t i = 0;
	if (Thread32First(snapshot_handle, thread_entry)) {
		do {
			if (thread_entry->th32OwnerProcessID == pid) {
				if (thread_list != NULL) {
					thread_list[i].id = thread_entry->th32ThreadID;
					thread_list[i].priority = thread_entry->tpBasePri;
				}
				++i;
			}
		}
		while (Thread32Next(snapshot_handle, thread_entry));
	}

	if (ret_thread_count != NULL)
		*ret_thread_count = i;

	free(thread_entry);
}

void FreeMyModuleList(struct MyModule* module_list, size_t module_count) {
	for (size_t i = 0; i < module_count; ++i) {
		free(module_list[i].exe_path);
		free(module_list[i].name);
	}
	free(module_list);
}
