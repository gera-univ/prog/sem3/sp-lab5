#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <strsafe.h>

#include "framework.h"


void DisplayVolumePaths(
	__in PWCHAR VolumeName
) {
	DWORD CharCount = MAX_PATH + 1;
	PWCHAR VolumeNames;
	BOOL Success;

	for (;;) {
		VolumeNames = (PWCHAR)malloc(sizeof(BYTE) * CharCount * sizeof(WCHAR));

		if (!VolumeNames) {
			return;
		}

		Success = GetVolumePathNamesForVolumeNameW(
			VolumeName, VolumeNames, CharCount, &CharCount
		);

		if (Success) {
			break;
		}

		if (GetLastError() != ERROR_MORE_DATA) {
			break;
		}

		free(VolumeNames);
	}

	if (Success) {
		PWCHAR NameIdx = NULL;
		for (NameIdx = VolumeNames;
		     NameIdx[0] != L'\0';
		     NameIdx += wcslen(NameIdx) + 1) {
			_tprintf(L"  %s", NameIdx);
		}
		_tprintf("\n");
	}

	if (VolumeNames != NULL) {
		free(VolumeNames);
	}
}

__declspec(dllexport) void __cdecl Execute() {
	DWORD CharCount = 0;
	WCHAR DeviceName[MAX_PATH] = L"";
	HANDLE FindHandle = INVALID_HANDLE_VALUE;
	BOOL Found = FALSE;
	size_t Index = 0;
	BOOL Success = FALSE;
	WCHAR VolumeName[MAX_PATH] = L"";

	FindHandle = FindFirstVolumeW(VolumeName, ARRAYSIZE(VolumeName));

	if (FindHandle == INVALID_HANDLE_VALUE) {
		DWORD Error = GetLastError();
		wprintf(L"FindFirstVolumeW failed with error code %d\n", Error);
		return;
	}

	for (;;) {
		Index = wcslen(VolumeName) - 1;

		if (VolumeName[0] != L'\\' ||
			VolumeName[1] != L'\\' ||
			VolumeName[2] != L'?' ||
			VolumeName[3] != L'\\' ||
			VolumeName[Index] != L'\\') {
			_tprintf("FindFirstVolumeW/FindNextVolumeW returned a bad path: %ls\n", VolumeName);
			break;
		}

		VolumeName[Index] = L'\0';

		CharCount = QueryDosDeviceW(&VolumeName[4], DeviceName, ARRAYSIZE(DeviceName));

		VolumeName[Index] = L'\\';

		if (CharCount == 0) {
			DWORD Error = GetLastError();
			_tprintf("QueryDosDeviceW failed with error code %lu\n", Error);
			break;
		}

		_tprintf("\nFound a device:\n %ls", DeviceName);
		_tprintf("\nVolume name: %ls", VolumeName);
		_tprintf("\nPaths:");
		DisplayVolumePaths(VolumeName);

		Success = FindNextVolumeW(FindHandle, VolumeName, ARRAYSIZE(VolumeName));

		if (!Success) {
			DWORD Error = GetLastError();

			if (Error != ERROR_NO_MORE_FILES) {
				wprintf(L"FindNextVolumeW failed with error code %d\n", Error);
				break;
			}

			break;
		}
	}

	FindVolumeClose(FindHandle);
}

char authorMessage[] = "Herman <herman@spiralarms.org>";
char descriptionMessage[] = "Lists volumes";

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message);

__declspec(dllexport) BOOLEAN __cdecl GetAuthor(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, authorMessage);
}

__declspec(dllexport) BOOLEAN __cdecl GetDescription(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, descriptionMessage);
}

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message) {
	HRESULT result = StringCchCopy(buffer, dwBufferSize, message);
	*pdwBytesWritten = min(dwBufferSize, strlen(message) * sizeof(char));
	return result == S_OK;
}
