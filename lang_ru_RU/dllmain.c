#include "framework.h"

__declspec(dllexport) char __cdecl Lang_Name[] = "ru_RU";
__declspec(dllexport) char __cdecl Loc_PluginInfo[] = "Информация о плагинах:";
__declspec(dllexport) char __cdecl Loc_ExecutingPlugins[] = "Выполнение плагинов:";
__declspec(dllexport) char __cdecl Loc_LanguageSet[] = "Установлен язык:";
__declspec(dllexport) char __cdecl Loc_NumLoadedPlugins[] = "Загружено плагинов:";
__declspec(dllexport) char __cdecl Loc_LoadingPlugins[] = "Загружаются плагины";