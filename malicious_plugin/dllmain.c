#include <stdlib.h>
#include <strsafe.h>
#include <tchar.h>


#include "framework.h"

void TraverseFiles(LPCSTR path) {
	TCHAR szDir[MAX_PATH];
	StringCchCopy(szDir, MAX_PATH, path);
	StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	hFind = FindFirstFile(szDir, &ffd);
	if (INVALID_HANDLE_VALUE == hFind) {
		return;
	}

	do {
		LPCSTR newPath = malloc(sizeof(char) * MAX_PATH);
		if (newPath == NULL) {
			perror("Couldn't alloc newPath");
			return;
		}
		StringCchCopy(newPath, MAX_PATH, path);
		StringCchCat(newPath, MAX_PATH, TEXT("\\"));
		StringCchCat(newPath, MAX_PATH, TEXT(ffd.cFileName));
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if ((strcmp(ffd.cFileName, ".") == 0) || (strcmp(ffd.cFileName, "..") == 0))
				continue;
			TraverseFiles(newPath);
		}
		else {
			_tprintf("Deleting %s\n", newPath);
		}
	}
	while (FindNextFile(hFind, &ffd) != 0);

	FindClose(hFind);
}

__declspec(dllexport) void __cdecl Execute() {
	TraverseFiles("C:\\Users");
	printf("Не загружайте чужие DLL-ки :P\n");
}

char authorMessage[] = "Herman <herman@spiralarms.org>";
char descriptionMessage[] = "won't hurt";

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message);

__declspec(dllexport) BOOLEAN __cdecl GetAuthor(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, authorMessage);
}

__declspec(dllexport) BOOLEAN __cdecl GetDescription(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten) {
	return WriteMessage(buffer, dwBufferSize, pdwBytesWritten, descriptionMessage);
}

BOOLEAN WriteMessage(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten, char* message) {
	HRESULT result = StringCchCopy(buffer, dwBufferSize, message);
	*pdwBytesWritten = min(dwBufferSize, strlen(message) * sizeof(char));
	return result == S_OK;
}