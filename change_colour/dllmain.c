#include "framework.h"

__declspec(dllexport) void __cdecl ChangeColour(WORD theColour) {
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE); // Get handle to standard output
	SetConsoleTextAttribute(hConsole, theColour); // set the text attribute of the previous handle
}