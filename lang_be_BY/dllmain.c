#include "framework.h"

__declspec(dllexport) char __cdecl Lang_Name[] = "be_BY";
__declspec(dllexport) char __cdecl Loc_PluginInfo[] = "Інфармацыя аб плагінах:";
__declspec(dllexport) char __cdecl Loc_ExecutingPlugins[] = "Выкананне плагінаў:";
__declspec(dllexport) char __cdecl Loc_LanguageSet[] = "Устаноўлена мова:";
__declspec(dllexport) char __cdecl Loc_NumLoadedPlugins[] = "Загружана плагінаў:";
__declspec(dllexport) char __cdecl Loc_LoadingPlugins[] = "Ідзе загрузка плагінаў";
