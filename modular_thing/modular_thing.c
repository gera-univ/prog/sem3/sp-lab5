#include <stdio.h>
#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <Shlwapi.h>
#include "../dll_lister/dll_lister.h"
#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "User32.lib")

extern __declspec(dllimport) void __cdecl ChangeColour(WORD theColour);

typedef int (__cdecl *ProcExecute)();
typedef int (__cdecl *ProcGetInfo)(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten);

WORD FOREGROUND_DEFAULT = FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE;

CHAR* GetThisPath(CHAR* dest, size_t destSize) {
	if (!dest) return NULL;
	if (MAX_PATH > destSize) return NULL;

	DWORD length = GetModuleFileName(NULL, dest, destSize);
	PathRemoveFileSpec(dest);
	return dest;
}

LPSTR getPluginDescription(HINSTANCE plugin) {
	ProcGetInfo procGetDescription = (ProcGetInfo)GetProcAddress(plugin, "GetDescription");
	LPSTR descriptionBuffer = malloc(512);
	if (descriptionBuffer == NULL) {
		perror("Couldn't allocate descriptionBuffer");
		return NULL;
	}
	DWORD descriptionBytesWritten;
	if (procGetDescription != NULL) {
		BOOLEAN result = (procGetDescription)(descriptionBuffer, 512, &descriptionBytesWritten);
		if (!result) {
			StringCchCopy(descriptionBuffer, 512, TEXT("Incorrect buffer size"));
		}
	}
	else {
		StringCchCopy(descriptionBuffer, 512, TEXT("GetDescription is not implemented"));
	}

	return descriptionBuffer;
}

void ExecutePlugins(HINSTANCE* loaded_plugins, size_t loaded_count) {
	for (size_t i = 0; i < loaded_count; ++i) {
		printf("Executing plugin: ");
		LPSTR descriptionBuffer = getPluginDescription(loaded_plugins[i]);
		if (descriptionBuffer != NULL) {
			_tprintf("%s", descriptionBuffer);
			free(descriptionBuffer);
		}
		else {
			_tprintf("Error");
		}
		printf("\n");

		ProcExecute function = (ProcExecute)GetProcAddress(loaded_plugins[i], "Execute");
		ChangeColour(FOREGROUND_GREEN);
		if (function != NULL) {
			function();
		}
		else {
			ChangeColour(FOREGROUND_INTENSITY);
			_tprintf("%s is not implemented\n", "Execute");
			ChangeColour(FOREGROUND_DEFAULT);
		}
		ChangeColour(FOREGROUND_DEFAULT);
	}
}

void PrintPluginInfo(HINSTANCE* loaded_plugins, size_t loaded_count) {
	ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	for (size_t i = 0; i < loaded_count; ++i) {
		ProcGetInfo getAuthor = (ProcGetInfo)GetProcAddress(loaded_plugins[i], "GetAuthor");

		LPSTR authorBuffer = malloc(256);
		DWORD authorBytesWritten;
		if (getAuthor != NULL) {
			BOOLEAN result = (getAuthor)(authorBuffer, 256, &authorBytesWritten);
			if (result) {
				_tprintf("%s", authorBuffer);
			}
			else {
				_tprintf("--");
			}
		}
		else {
			_tprintf("%s is not implemented", "GetAuthor");
		}
		printf("\t\t");
		free(authorBuffer);

		LPSTR descriptionBuffer = getPluginDescription(loaded_plugins[i]);
		if (descriptionBuffer != NULL) {
			printf("%s", descriptionBuffer);
			free(descriptionBuffer);
		}

		_tprintf("\n");
	}
	ChangeColour(FOREGROUND_DEFAULT);
}

void LoadPlugins(LPCSTR* dll_list, size_t dll_count, HINSTANCE* loaded_plugins, size_t* loaded_count) {
	*loaded_count = 0;
	printf("Found %zu DLLs\n", dll_count);
	for (size_t i = 0; i < dll_count; ++i) {
		char dll_name[MAX_PATH];
		StringCchCopy(dll_name, MAX_PATH, dll_list[i]);
		StringCchCat(dll_name, MAX_PATH, TEXT("."));
		HINSTANCE hinstLib = LoadLibrary(TEXT(dll_name));
		if (hinstLib != NULL)
			loaded_plugins[(*loaded_count)++] = hinstLib;
		else {
			ChangeColour(FOREGROUND_RED);
			_tprintf("Unable to load DLL %s\n", dll_list[i]);
			ChangeColour(FOREGROUND_DEFAULT);
		}
	}
}

void UnloadPlugins(HINSTANCE* loaded_plugins, size_t loaded_count) {
	for (size_t i = 0; i < loaded_count; i++) {
		FreeLibrary(loaded_plugins[i]);
	}
	free(loaded_plugins);
}

void PrintHelp() {
	ChangeColour(FOREGROUND_DEFAULT | FOREGROUND_INTENSITY);
	_tprintf(
		"View info about and execute DLL plugins.");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\n\nOptions:");
	ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	_tprintf("\n  -h, --help");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\t\tdisplay help message\n");
	ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	_tprintf("  -d, --dry-run");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\t\tdo not execute plugins, only print info, list languages even if the following option is unspecified\n");
	ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	_tprintf("  -L, --lang");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\t\tset the language, must be equal to the Lang_Name variable exported in the language plugin\n");
	ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	_tprintf("  --lang-dir");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\t\trelative path to the directory too search for language plugins in (default is \"lang\")\n");
	ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	_tprintf("  --plugin-dir");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\t\trelative path to the directory too search for plugins in (default is \"plugins\")\n");
	_tprintf("\n\nPlugin DLLs should export the following functions:");
	ChangeColour(FOREGROUND_INTENSITY);
	_tprintf(
		"\n__declspec(dllexport) BOOLEAN __cdecl GetAuthor(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten)\n\
__declspec(dllexport) BOOLEAN __cdecl GetDescription(LPSTR buffer, DWORD dwBufferSize, DWORD* pdwBytesWritten)\n\
__declspec(dllexport) void __cdecl Execute()\n");
	ChangeColour(FOREGROUND_DEFAULT);
	_tprintf("\n\nLanguage DLLs should export the following variables:");
	ChangeColour(FOREGROUND_INTENSITY);
	_tprintf(
		"\n__declspec(dllexport) char __cdecl Lang_Name[]\n\
__declspec(dllexport) char __cdecl Loc_PluginInfo[]\n\
__declspec(dllexport) char __cdecl Loc_LanguageSet[]\n\
__declspec(dllexport) char __cdecl Loc_NumLoadedPlugins[]\n\
__declspec(dllexport) char __cdecl Loc_LoadingPlugins[]\n");
	ChangeColour(FOREGROUND_DEFAULT);
}

char* Loc_PluginInfo;
char* Loc_ExecutingPlugins;
char* Loc_LanguageSet;
char* Loc_NumLoadedPlugins;
char* Loc_LoadingPlugins;

void AddTranslation(char** loc_variable, char* foreign_loc_variable, char loc_variable_name[]) {
	if (foreign_loc_variable == NULL) {
		_tprintf("No translation for %s\n", loc_variable_name);
	}
	else {
		if (*loc_variable != NULL) free(*loc_variable);
		*loc_variable = foreign_loc_variable;
	}
}

void InitLanguages(char const* targetLanguage, char const *lang_dir_new) {
	CHAR path[MAX_PATH];
	GetThisPath(path, MAX_PATH);
	if (lang_dir_new != NULL) {
		StringCchCat(path, MAX_PATH, TEXT("\\"));
		StringCchCat(path, MAX_PATH, lang_dir_new);
	} else {
		StringCchCat(path, MAX_PATH, TEXT("\\lang"));
	}	

	size_t dll_count = 0;
	ListDLLs(path, NULL, &dll_count);

	LPCSTR* dll_list = malloc(sizeof(LPCSTR) * dll_count);
	if (dll_list == NULL) {
		perror("InitLanguages: Unable to allocate dll_list");
		return;
	}
	ListDLLs(path, dll_list, NULL);

	HINSTANCE* plugin_list = malloc(sizeof(HINSTANCE) * dll_count);
	if (plugin_list == NULL) {
		perror("InitLanguages: Unable to allocate plugin_list");
		return;
	}

	size_t plugin_count;
	LoadPlugins(dll_list, dll_count, plugin_list, &plugin_count);

	_tprintf("Loaded %zu language plugins:\n", plugin_count);

	int iFoundLanguage = -1;
	ChangeColour(FOREGROUND_GREEN | FOREGROUND_INTENSITY);
	for (size_t i = 0; i < plugin_count; ++i) {
		char* procStrLangName = (char*)GetProcAddress(plugin_list[i], "Lang_Name");
		_tprintf("%s\n", procStrLangName);
		if (procStrLangName != NULL && targetLanguage != NULL && strcmp(procStrLangName, targetLanguage) == 0) {
			iFoundLanguage = i;
		}
	}
	ChangeColour(FOREGROUND_RED);
	if (iFoundLanguage == -1) {
		if (targetLanguage != NULL) {
			_tprintf("Unable to find language %s. Falling back to the original language\n", targetLanguage);
		}
	}
	else {
		char* procStrLoc_PluginInfo = (char*)GetProcAddress(plugin_list[iFoundLanguage], "Loc_PluginInfo");
		char* procStrLoc_ExecutingPlugins = (char*)GetProcAddress(plugin_list[iFoundLanguage], "Loc_ExecutingPlugins");
		char* procStrLoc_LanguageSet = (char*)GetProcAddress(plugin_list[iFoundLanguage], "Loc_LanguageSet");
		char* procStrLoc_NumLoadedPlugins = (char*)GetProcAddress(plugin_list[iFoundLanguage], "Loc_NumLoadedPlugins");
		char* procStrLoc_LoadingPlugins = (char*)GetProcAddress(plugin_list[iFoundLanguage], "Loc_LoadingPlugins");

		AddTranslation(&Loc_LanguageSet, procStrLoc_LanguageSet, "LanguageSet");
		AddTranslation(&Loc_PluginInfo, procStrLoc_PluginInfo, "PluginInfo");
		AddTranslation(&Loc_ExecutingPlugins, procStrLoc_ExecutingPlugins, "ExecutingPlugins");
		AddTranslation(&Loc_NumLoadedPlugins, procStrLoc_NumLoadedPlugins, "NumLoadedPlugins");
		AddTranslation(&Loc_LoadingPlugins, procStrLoc_LoadingPlugins, "LoadingPlugins");

		ChangeColour(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		_tprintf("%s %s\n", Loc_LanguageSet, targetLanguage);
	}
	ChangeColour(FOREGROUND_DEFAULT);
}

void AddDefaultTranslation(char** loc_string, char value[], char loc_string_name[]) {
	const size_t szValue = strlen(value) + 1;
	*loc_string = malloc(szValue * sizeof(char));
	if (*loc_string == NULL) {
		_tprintf("Unable to allocate %s", loc_string_name);
		return;
	}

	strcpy_s(*loc_string, szValue, value);
}

void InitDefaultLanguage() {
	AddDefaultTranslation(&Loc_PluginInfo, "Plugin info:", "PluginInfo");
	AddDefaultTranslation(&Loc_ExecutingPlugins, "Executing plugins:", "ExecutingPlugins");
	AddDefaultTranslation(&Loc_LanguageSet, "Language has been set:", "LanguageSet");
	AddDefaultTranslation(&Loc_NumLoadedPlugins, "Loaded plugins:", "NumLoadedPlugins");
	AddDefaultTranslation(&Loc_LoadingPlugins, "Loading plugins", "LoadingPlugins");
}

int main(int argc, char* argv[]) {
	BOOLEAN is_dry_run = FALSE;
	BOOLEAN waiting_for_language = FALSE;
	BOOLEAN waiting_for_new_plugin_dir = FALSE;
	BOOLEAN waiting_for_new_lang_dir = FALSE;
	char* language = NULL;
	char* plugin_dir_new = NULL;
	char* lang_dir_new = NULL;
	for (int argi = 1; argi < argc; ++argi) {
		if (waiting_for_language) {
			language = argv[argi];
			waiting_for_language = FALSE;
			continue;
		}
		if (waiting_for_new_plugin_dir) {
			plugin_dir_new = argv[argi];
			waiting_for_new_plugin_dir = FALSE;
			continue;
		}
		if (waiting_for_new_lang_dir) {
			lang_dir_new = argv[argi];
			waiting_for_new_lang_dir = FALSE;
			continue;
		}
		if (strcmp(argv[argi], "-h") == 0 || strcmp(argv[argi], "--help") == 0) {
			PrintHelp();
			return 0;
		}
		if (strcmp(argv[argi], "-d") == 0 || strcmp(argv[argi], "--dry-run") == 0) {
			is_dry_run = TRUE;
		}
		if (strcmp(argv[argi], "-L") == 0 || strcmp(argv[argi], "--lang") == 0) {
			waiting_for_language = TRUE;
		}
		if (strcmp(argv[argi], "--plugin-dir") == 0) {
			waiting_for_new_plugin_dir = TRUE;
		}
		if (strcmp(argv[argi], "--lang-dir") == 0) {
			waiting_for_new_lang_dir = TRUE;
		}
	}
	ChangeColour(FOREGROUND_RED);
	if (waiting_for_language) {
		_tprintf("No language specified\n");
	}
	if (waiting_for_new_plugin_dir) {
		_tprintf("No plugin dir specified\n");
	}
	if (waiting_for_new_lang_dir) {
		_tprintf("No lang dir specified\n");
	}
	ChangeColour(FOREGROUND_DEFAULT);

	InitDefaultLanguage();
	if (language != NULL || is_dry_run) {
		printf("Searching for languages\n");
		InitLanguages(language, lang_dir_new);
	}

	printf("%s\n", Loc_LoadingPlugins);
	CHAR path[MAX_PATH];
	GetThisPath(path, MAX_PATH);
	if (plugin_dir_new != NULL) {
		StringCchCat(path, MAX_PATH, TEXT("\\"));
		StringCchCat(path, MAX_PATH, plugin_dir_new);
	} else {
		StringCchCat(path, MAX_PATH, TEXT("\\plugins"));
	}

	size_t dll_count = 0;
	ListDLLs(path, NULL, &dll_count);

	LPCSTR* dll_list = malloc(sizeof(LPCSTR) * dll_count);
	if (dll_list == NULL) {
		perror("Unable to allocate dll_list");
		return 1;
	}
	ListDLLs(path, dll_list, NULL);

	HINSTANCE* plugin_list = malloc(sizeof(HINSTANCE) * dll_count);
	if (plugin_list == NULL) {
		perror("Unable to allocate plugin_list");
		return 1;
	}

	size_t plugin_count;
	LoadPlugins(dll_list, dll_count, plugin_list, &plugin_count);

	_tprintf("%s %zu\n", Loc_NumLoadedPlugins, plugin_count);

	_tprintf("%s\n", Loc_PluginInfo);
	PrintPluginInfo(plugin_list, plugin_count);

	if (!is_dry_run) {
		_tprintf("%s\n", Loc_ExecutingPlugins);
		ExecutePlugins(plugin_list, plugin_count);
	}

	UnloadPlugins(plugin_list, plugin_count);
	free(dll_list);

	return 0;
}
